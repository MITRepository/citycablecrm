﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using Acr.UserDialogs;
using CityCableCRM.Common.Constants;
using CityCableCRM.Common.Enumerators;
using CityCableCRM.Helpers;
using CityCableCRM.Model;
using CityCableCRM.Views;
using Newtonsoft.Json;
using Prism.Navigation;
using Xamarin.Forms;

namespace CityCableCRM.ViewModels
{
    public class DashboardPageViewModel : BaseViewModel
    {
        #region Data Members
        private readonly INavigationService _navigationService;
        #endregion

        #region Constructor
        public DashboardPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            DashboardModel = new DashboardModel();
            OnPagePrepration();
        }
        #endregion

        #region Properties
        private DashboardModel _dashboardModel;
        public DashboardModel DashboardModel
        {
            get { return _dashboardModel; }
            set { _dashboardModel = value; OnPropertyChanged(); }
        }

        private string _rentalBalance;
        public string RentalBalance
        {
            get{return _rentalBalance;}
            set { _rentalBalance = value; OnPropertyChanged("RentalBalance"); }
        }

        #endregion

        #region Methods
       private async void OnPagePrepration()
        {
            HttpClientHelper apicall = new HttpClientHelper("http://" + Settings.CCC_OperatorUrl + ApiUrls.Url_GetCustomerPaymentDetail, string.Empty);
            DashboardModel.customerid = Settings.CCC_CustomerId;
            DashboardModel.month = DateTime.Now.ToString("MMMM");
            DashboardModel.year = DateTime.Now.ToString("yyyy");
            var rentaljson = JsonConvert.SerializeObject(DashboardModel);
            var response = await apicall.Post<DashboardResponse>(rentaljson);
            if (response?.status != null && response.status == "true")
            {
                Settings.CCC_CustomerName = response.data[0].cust_name;
                Settings.CCC_CustomerAddress = response.data[0].cust_address;
                Settings.CCC_CustomerMobile = response.data[0].cust_mobile;
                Settings.CCC_CustomerAccountNumber = response.data[0].CustomerACNo;
                Settings.CCC_CustomerLocation = response.data[0].cust_location;
                RentalBalance = "Pending Renal for " + response.data[0].month+ " : " + response.data[0].monthly_rental.Substring(0, 6);

                DisposeObject();
                UserDialogs.Instance.Loading().Hide();
            }
        }
        private void DisposeObject()
        {
            DashboardModel = new DashboardModel();
        }
        #endregion
    }
}
