﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using CityCableCRM.Common.Constants;
using CityCableCRM.Helpers;
using CityCableCRM.Model;
using Newtonsoft.Json;
using Prism.Navigation;
using Xamarin.Forms;

namespace CityCableCRM.ViewModels
{
    public class OpenTicketPageViewModel : BindableBase
    {
        #region Data Members
        private readonly INavigationService _navigationService;
        #endregion

        #region Constructor
        public OpenTicketPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            OpenTicketModel = new OpenTicketModel();
            CreateOpenTicketModel = new CreateOpenTicketModel();
            OnPagePrepration();
            SubmitTicket = new DelegateCommand<ShowOpenTicket>(OnSubmitClickedAsync);
        }
        #endregion

        #region Properties
        private ObservableCollection<ShowOpenTicket> _ticketTypeItems = new ObservableCollection<ShowOpenTicket>();
        public ObservableCollection<ShowOpenTicket> TicketTypeItems
        {
            get { return _ticketTypeItems; }
            set { _ticketTypeItems = value; OnPropertyChanged("TicketTypeItems"); }
        }

        private OpenTicketModel _openTicketModel;
        public OpenTicketModel OpenTicketModel
        {
            get { return _openTicketModel; }
            set { _openTicketModel = value; OnPropertyChanged(); }
        }

        private string statusMessage;
        public string StatusMessage
        {
            get { return statusMessage; }
            set { statusMessage = value; OnPropertyChanged(); }
        }

        private bool isStatusVisible;
        public bool IsStatusVisible
        {
            get { return isStatusVisible; }
            set { isStatusVisible = value; OnPropertyChanged(); }
        }

        private CreateOpenTicketModel _createOpenTicketModel;
        public CreateOpenTicketModel CreateOpenTicketModel
        {
            get { return _createOpenTicketModel; }
            set { _createOpenTicketModel = value; OnPropertyChanged(); }
        }
        #endregion

        #region DelegateCommand
        public DelegateCommand<ShowOpenTicket> SubmitTicket { get; set; }
        #endregion

        #region Methods
        private async void OnPagePrepration()
        {
            TicketTypeItems = await GetTicketTypeList();
        }
        private async Task<ObservableCollection<ShowOpenTicket>> GetTicketTypeList()
        {
            IsStatusVisible = false;
            var tickets = new ObservableCollection<ShowOpenTicket>();
            HttpClientHelper apicall = new HttpClientHelper("http://" + Settings.CCC_OperatorUrl + ApiUrls.Url_TicketType, string.Empty);
            var ticketjson = JsonConvert.SerializeObject(OpenTicketModel);
            var response = await apicall.Post<OpenTicketResponse>(ticketjson);
            if (response?.status != null && response.status == "true")
            {
                foreach (var ticket in response.data)
                {
                    tickets.Add(new ShowOpenTicket()
                    {
                        TicketId = ticket.ticket_id,
                        TicketType = ticket.ticket_type
                    });
                }
                DisposeObject();
            }
            else
            {
                IsStatusVisible = true;
                StatusMessage = response.message;
            }
            return tickets;
        }
        private void DisposeObject()
        {
            OpenTicketModel = new OpenTicketModel();
            CreateOpenTicketModel = new CreateOpenTicketModel();
        }
        private async void OnSubmitClickedAsync(ShowOpenTicket showTicketTypes)
        {
            string id = showTicketTypes.TicketId;

            HttpClientHelper apicall = new HttpClientHelper("http://" + Settings.CCC_OperatorUrl + ApiUrls.Url_OpenTicket, string.Empty);
            CreateOpenTicketModel.tickettypeid = id;
            CreateOpenTicketModel.addedby = "1";
            CreateOpenTicketModel.reporteduserid = "14";
            CreateOpenTicketModel.status = "Open";
            CreateOpenTicketModel.updatedby = "1";
            var createopenticketjson = JsonConvert.SerializeObject(CreateOpenTicketModel);
            var response = await apicall.Post<CreateOpenTicketResponse>(createopenticketjson);
            if (response != null)
            {
                if (response.status != null && response.status == "true")
                {
                    await Application.Current.MainPage.DisplayAlert("Success", "Ticket Created SuccessFully...", "Ok");
                    await _navigationService.NavigateAsync("GetMyTicketDetailPage", null, null, false);
                    DisposeObject();
                }
            }
        }
        #endregion

    }
}
