﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Acr.UserDialogs;
using CityCableCRM.Common.Constants;
using CityCableCRM.Helpers;
using CityCableCRM.Model;
using Newtonsoft.Json;
using Prism.Navigation;

namespace CityCableCRM.ViewModels
{
    public class PaidBillsPageViewModel : BindableBase
    {
        #region Data Members
        private readonly INavigationService _navigationService;
        #endregion

        #region Constructor
        public PaidBillsPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            PaidBillsModel = new PaidBillsModel();
            OnPagePrepration();
            ItemsList = new List<string>
            {
                DateTime.Now.AddYears(-1).Year.ToString(),
                DateTime.Now.Year.ToString(),
                DateTime.Now.AddYears(1).Year.ToString()
            };
            SubmitYear = new DelegateCommand(OnSubmitButtonClickedAsync);
        }
        #endregion

        #region Properties

        private List<string> _itemsList;
        public List<string> ItemsList
        {
            get { return _itemsList; }
            set { _itemsList = value; OnPropertyChanged("ItemsList"); }
        }

        private ObservableCollection<ShowPaidBills> _paidBills = new ObservableCollection<ShowPaidBills>();
        public ObservableCollection<ShowPaidBills> PaidBills
        {
            get { return _paidBills; }
            set { _paidBills = value; OnPropertyChanged("PaidBills"); }
        }

        private PaidBillsModel _paidBillsModel;
        public PaidBillsModel PaidBillsModel
        {
            get { return _paidBillsModel; }
            set { _paidBillsModel = value; OnPropertyChanged(); }
        }

        private object _itemSelectedFromList;
        public object ItemSelectedFromList
        {
            get { return _itemSelectedFromList; }
            set { _itemSelectedFromList = value; OnPropertyChanged("ItemSelectedFromList"); }
        }

        private string statusMessage;
        public string StatusMessage
        {
            get { return statusMessage; }
            set { statusMessage = value; OnPropertyChanged(); }
        }

        private bool isStatusVisible;
        public bool IsStatusVisible
        {
            get { return isStatusVisible; }
            set { isStatusVisible = value; OnPropertyChanged(); }
        }
        #endregion

        #region DelegateCommand
        public DelegateCommand SubmitYear { get; set; }
        #endregion

        #region Methods
        private async void OnPagePrepration()
        {
            if (ItemSelectedFromList == null)
                ItemSelectedFromList = DateTime.Now.Year.ToString();

            PaidBills = await GetPaidBills(ItemSelectedFromList.ToString());
        }
        private async Task<ObservableCollection<ShowPaidBills>> GetPaidBills(string year)
        {
            UserDialogs.Instance.Loading("please wait..");
            IsStatusVisible = false;
            var bills = new ObservableCollection<ShowPaidBills>();
            HttpClientHelper apicall = new HttpClientHelper("http://" + Settings.CCC_OperatorUrl + ApiUrls.Url_PaidBill, string.Empty);
            PaidBillsModel.customerid = Settings.CCC_CustomerId;
            PaidBillsModel.year = year;
            var paidbillsjson = JsonConvert.SerializeObject(PaidBillsModel);
            var response = await apicall.Post<PaidBillsResponse>(paidbillsjson);
            if (response?.status != null && response.status == "true")
            {
                for (int count = 0; count < response.data.Count(); count++)
                {
                    bills.Add(new ShowPaidBills()
                    {
                        Month = response.data[count].month,
                        BillNumber = response.data[count].billnumber,
                        PaidAmount = response.data[count].paidamount
                    });
                }
                if (bills.Count == 0)
                {
                    IsStatusVisible = true;
                    StatusMessage = response.message;
                    UserDialogs.Instance.Loading().Hide();
                }
                DisposeObject();
                UserDialogs.Instance.Loading().Hide();
            }
            else
            {
                IsStatusVisible = true;
                StatusMessage = response.message;
                UserDialogs.Instance.Loading().Hide();
            }
            return bills;
        }
        private void DisposeObject()
        {
            PaidBillsModel = new PaidBillsModel();
        }

        private async void OnSubmitButtonClickedAsync()
        {
            await GetPaidBills(ItemSelectedFromList.ToString());
        }
        #endregion
    }
}
