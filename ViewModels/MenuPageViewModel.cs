﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CityCableCRM.Helpers;
using CityCableCRM.Model;
using Prism.Navigation;
using Xamarin.Forms;

namespace CityCableCRM.ViewModels
{
    public class MenuPageViewModel : BaseViewModel
    {
        private readonly INavigationService _navigationService;

        #region Constructor
        public MenuPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            MenuList = MenuListItems();
            PagesListCommand = new DelegateCommand(OnPageListClickAsync);

            DisplayName = Settings.CCC_CustomerName;
            ProfilePicImageSource = "http://" + Settings.CCC_ProfilePictureUrl;
        }
        #endregion

        #region DelegateCommand
        public DelegateCommand PagesListCommand { get; set; }
        #endregion

        #region Properties
        public ObservableCollection<MenuModel> MenuList { get; set; }

        private MenuModel _listSelectedItem;
        public MenuModel ListSelectedItem
        {
            get { return _listSelectedItem; }
            set { SetProperty(ref _listSelectedItem, value); OnPropertyChanged(); }
        }

        private string _profilePicImageSource;
        public string ProfilePicImageSource
        {
            get { return _profilePicImageSource; }
            set { _profilePicImageSource = value; OnPropertyChanged("ProfilePicImageSource"); }
        }

        private string _displayName;
        public string DisplayName
        {
            get { return _displayName; }
            set { _displayName = value; OnPropertyChanged("DisplayName"); }
        }
        #endregion

        #region Methods
        private ObservableCollection<MenuModel> MenuListItems()
        {
            ObservableCollection<MenuModel> reports = new ObservableCollection<MenuModel>
            {
                new MenuModel
                {
                   MenuItem = "Dashboard",
                   PageName = "DashboardPage"
                },
                new MenuModel
                {
                   MenuItem = "Open Tickets",
                   PageName = "GetMyTicketDetailPage"
                },
                new MenuModel
                {
                   MenuItem = "Paid Bills",
                   PageName = "PaidBillsPage"
                },
                new MenuModel
                {
                   MenuItem = "My Profile",
                   PageName = "MyProfilePage"
                },
                new MenuModel
                {
                   MenuItem = "Pending Payments",
                   PageName = "PendingPaymentsPage"
                },
                new MenuModel
                {
                   MenuItem = "Log Out",
                   PageName = "LoginPage"
                }
            };
            return reports;
        }
        private async void OnPageListClickAsync()
        {
            string name = ListSelectedItem.PageName;
            await _navigationService.NavigateAsync("MainMasterDetailPage/CommonNavigationPage/" + name, null, null, false);
        }
        #endregion
    }
}
