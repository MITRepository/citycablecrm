﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Acr.UserDialogs;
using CityCableCRM.Common.Constants;
using CityCableCRM.Helpers;
using CityCableCRM.Model;
using Newtonsoft.Json;
using Prism.Navigation;

namespace CityCableCRM.ViewModels
{
    public class GetMyTicketDetailPageViewModel : BaseViewModel
    {
        #region Data Members
        private readonly INavigationService _navigationService;
        #endregion

        #region Constructor
        public GetMyTicketDetailPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            GetMyTicketDetailModel = new GetMyTicketDetailModel();
            OnPagePrepration();
            NewOpenTicket = new DelegateCommand(OnLoginButtonClickedAsync);
        }
        #endregion

        #region DelegateCommand
        public DelegateCommand NewOpenTicket { get; set; }
        #endregion

        #region Properties
        private ObservableCollection<ShowMyTicketDetail> _myTickets = new ObservableCollection<ShowMyTicketDetail>();
        public ObservableCollection<ShowMyTicketDetail> MyTickets
        {
            get { return _myTickets; }
            set { _myTickets = value; OnPropertyChanged("MyTickets"); }
        }

        private GetMyTicketDetailModel _getMyTicketDetailModel;
        public GetMyTicketDetailModel GetMyTicketDetailModel
        {
            get { return _getMyTicketDetailModel; }
            set { _getMyTicketDetailModel = value; OnPropertyChanged(); }
        }
        #endregion

        #region Methods
        private async void OnPagePrepration()
        {
            MyTickets = await GetMyTickets();
        }
        private async Task<ObservableCollection<ShowMyTicketDetail>> GetMyTickets()
        {
            UserDialogs.Instance.Loading("please wait..");
            var tickets = new ObservableCollection<ShowMyTicketDetail>();
            HttpClientHelper apicall = new HttpClientHelper("http://" + Settings.CCC_OperatorUrl + ApiUrls.Url_GetMyTicketDetails, string.Empty);
            GetMyTicketDetailModel.rolename = "Customer";
            GetMyTicketDetailModel.reporteduserid = "14";
            GetMyTicketDetailModel.assigneduserid = "";
            GetMyTicketDetailModel.status = "Open";
            GetMyTicketDetailModel.pageindex = "1";
            GetMyTicketDetailModel.pagesize = "10";
            GetMyTicketDetailModel.recordcount = "50";
            var myticketjson = JsonConvert.SerializeObject(GetMyTicketDetailModel);
            var response = await apicall.Post<GetMyTicketDetailResponse>(myticketjson);
            if (response?.status != null && response.status == "true")
            {
                for (int count = 0; count < response.data.Count(); count++)
                {
                    tickets.Add(new ShowMyTicketDetail()
                    {
                        TicketIssue = response.data[count].mytickets_ticketnumber + "-" + response.data[count].mytickets_isuuetype,
                        StatusAssignedTo = response.data[count].mytickets_status + "-" + response.data[count].mytickets_assignedusername
                    });
                }
                DisposeObject();
                UserDialogs.Instance.Loading().Hide();
            }
            return tickets;
        }
        private void DisposeObject()
        {
            GetMyTicketDetailModel = new GetMyTicketDetailModel();
        }
        private async void OnLoginButtonClickedAsync()
        {
            UserDialogs.Instance.Loading("please wait..");
            await _navigationService.NavigateAsync("OpenTicketPage", null, null, false);
            UserDialogs.Instance.Loading().Hide();
        }
        #endregion
    }
}
