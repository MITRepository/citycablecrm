﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using CityCableCRM.Common.Constants;
using CityCableCRM.Helpers;
using CityCableCRM.Model;
using Newtonsoft.Json;
using Prism.Navigation;
using Xamarin.Forms;

namespace CityCableCRM.ViewModels
{
    public class LoginPageViewModel : BaseViewModel
    {
        #region Data Members
        private readonly INavigationService _navigationService;
        #endregion

        #region Constructor
        public LoginPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            LoginCommand = new DelegateCommand(OnLoginButtonClickedAsync);
            UserLogin = new LoginModel();
        }
        #endregion

        #region DelegateCommand
        public DelegateCommand LoginCommand { get; set; }
        #endregion

        #region Properties
        private LoginModel userlogin;
        public LoginModel UserLogin
        {
            get { return userlogin; }
            set { userlogin = value; OnPropertyChanged(); }
        }

        private bool _rememberMe;
        public bool RememberMe
        {
            get { return _rememberMe; }
            set
            {
                SetProperty(ref _rememberMe, value);
                OnPropertyChanged();
            }
        }
        #endregion

        #region Methods
        private async void OnLoginButtonClickedAsync()
        {
            if (string.IsNullOrWhiteSpace(UserLogin.username) || string.IsNullOrWhiteSpace(UserLogin.password))
            {
                await Application.Current.MainPage.DisplayAlert("Login", "Please enter the Email Id and Password", "OK");
                return;
            }

            if (!IsRunningTasks)
            {
                IsRunningTasks = true;
                HttpClientHelper apicall = new HttpClientHelper("http://" + Settings.CCC_OperatorUrl + ApiUrls.Url_Login, string.Empty);
                var loginjson = JsonConvert.SerializeObject(UserLogin);
                var response = await apicall.Post<LoginResponse>(loginjson);
                if (response != null)
                {
                    if (response.status != null && response.status == "true")
                    {
                        Settings.CCC_ProfilePictureUrl = Settings.CCC_OperatorUrl + response.data.profilepictureurl;
                        Settings.CCC_CustomerId = response.data.customerid;
                        await _navigationService.NavigateAsync("MainMasterDetailPage/CommonNavigationPage/DashboardPage", null, null, false);
                        DisposeObject();
                    }
                    else if (response.status != null && response.status == "false")
                    {
                        await Application.Current.MainPage.DisplayAlert("Login", response.message, "OK"); ;
                    }
                    else
                    {
                        await Application.Current.MainPage.DisplayAlert("Login", AppConstant.LOGIN_FAILURE, "OK");
                    }
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Login", AppConstant.LOGIN_FAILURE, "OK");
                }
            }
            IsRunningTasks = false;
        }
        private void DisposeObject()
        {
            UserLogin = new LoginModel();
        }
        #endregion
    }
}
