﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityCableCRM.Common.Constants
{
    public class ApiUrls
    {
        public const string Base_Url = "http://smartdish.checkyourproject.com";
        public const string Url_UserVerify = Base_Url + "/DesktopModules/MIT_Dish_WebApi/API/Service/AuthenticateAppUser";
        public const string Url_Login = "/DesktopModules/MIT_Dish_WebApi/API/Service/CustomerAppLogin";
        public const string Url_GetMyTicketDetails = "/DesktopModules/MIT_Dish_WebApi/API/Service/GetMyTicketDetails";
        public const string Url_GetCustomerPaymentDetail = "/DesktopModules/MIT_Dish_WebApi/API/Service/GetCustomerPaymentDetail";
        public const string Url_PaidBill = "/DesktopModules/MIT_Dish_WebApi/API/Service/GetPaidRentalDetailsOfCustomer";

        public const string Url_TicketType = "/DesktopModules/MIT_Dish_WebApi/API/Service/GetAllTicketType";
        public const string Url_OpenTicket = "/DesktopModules/MIT_Dish_WebApi/API/Service/OpenTicket";

    }
}
