﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityCableCRM.Common.Constants
{
    public class AppConstant
    {
        public const string APP_TITLE = "City CalbleCRM";

        public const string EMPTY_STRING = "";
        public const string NETWORK_FAILURE = "No Network Connection found, Please try again";
        public const string LOGIN_FAILURE = "Login Failed! Please try again";
        public const string VERIFICATION_FAILURE = "Verification Failed! You are entering wrong school code,please enter valid code";
        public const string REGISTRATION_FAILURE = "Registration Failed! Please try again";
    }
}
