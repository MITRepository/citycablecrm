using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace CityCableCRM.Helpers
{
  public static class Settings
  {
    private static ISettings AppSettings
    {
      get
      {
        return CrossSettings.Current;
      }
    }

        #region Setting Constants
        private const string CustomerName = "Customer_Name";
        private static readonly string DefaultCustomerName = string.Empty;
        private const string CustomerAddress = "Customer_Address";
        private static readonly string DefaultCustomerAddress = string.Empty;
        private const string CustomerMobile = "Customer_Mobile";
        private static readonly string DefaultCustomerMobile = string.Empty;
        private const string CustomerLocation = "Customer_Location";
        private static readonly string DefaultCustomerLocation = string.Empty;
        private const string CustomerAccountNumber = "Customer_Account_Number";
        private static readonly string DefaultCustomerAccountNumber = string.Empty;
        private const string OperatorUrl = "Operator_Url";
        private static readonly string DefaultOperatorUrl = string.Empty;
        private const string OperatorCode = "Operator_Code";
        private static readonly string DefaultOperatorCode = string.Empty;
        private const string ProfilePictureUrl = "Profile_Picture_Url";
        private static readonly string DefaultProfilePictureUrl = string.Empty;
        private const string CustomerId = "Customer_Id";
        private static readonly string DefaultCustomerId = string.Empty;

        #endregion

        #region Settings Properties
        public static string CCC_CustomerName
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(CustomerName, DefaultCustomerName);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(CustomerName, value);
            }
        }
        public static string CCC_CustomerAddress
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(CustomerAddress, DefaultCustomerAddress);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(CustomerAddress, value);
            }
        }
        public static string CCC_CustomerMobile
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(CustomerMobile, DefaultCustomerMobile);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(CustomerMobile, value);
            }
        }
        public static string CCC_CustomerLocation
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(CustomerLocation, DefaultCustomerLocation);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(CustomerLocation, value);
            }
        }
        public static string CCC_CustomerAccountNumber
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(CustomerAccountNumber, DefaultCustomerAccountNumber);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(CustomerAccountNumber, value);
            }
        }
        public static string CCC_OperatorUrl
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(OperatorUrl, DefaultOperatorUrl);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(OperatorUrl, value);
            }
        }
        public static string CCC_OperatorCode
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(OperatorCode, DefaultOperatorCode);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(OperatorCode, value);
            }
        }
        public static string CCC_ProfilePictureUrl
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(ProfilePictureUrl, DefaultProfilePictureUrl);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(ProfilePictureUrl, value);
            }
        }
        public static string CCC_CustomerId
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(CustomerId, DefaultCustomerId);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(CustomerId, value);
            }
        }
        #endregion
    }
}