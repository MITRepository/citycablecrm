﻿using System;
using System.Threading.Tasks;
using CityCableCRM.Common.Enumerators;
using Xamarin.Forms;

namespace CityCableCRM.Views
{
    public partial class DashboardPage : ContentPage
    {
        public DashboardPage()
        {
            InitializeComponent();
        }
        private async void OnClickSend(object sender, EventArgs e)
        {
            try
            {
                var arg = e as TappedEventArgs;
                var param = arg.Parameter as string;
                if (string.IsNullOrEmpty(param))
                    return;
                var pageType = (ApplicationActivity)Enum.Parse(typeof(ApplicationActivity), param);
                await PageNavigation(pageType);
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", ex.Message, "Ok");
            }
        }
        private async Task PageNavigation(ApplicationActivity page)
        {
            try
            {
                switch (page)
                {
                    case ApplicationActivity.GetMyTicketDetailPage:
                        await Navigation.PushAsync(new GetMyTicketDetailPage());
                        break;
                    case ApplicationActivity.PaidBillsPage:
                        await Navigation.PushAsync(new PaidBillsPage());
                        break;
                    case ApplicationActivity.MyProfilePage:
                        await Navigation.PushAsync(new MyProfilePage());
                        break;
                    case ApplicationActivity.PendingPaymentsPage:
                        await Navigation.PushAsync(new PendingPaymentsPage());
                        break;
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", ex.Message, "Ok");
                throw;
            }
        }
    }
}
