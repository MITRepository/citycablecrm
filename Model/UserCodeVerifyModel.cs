﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityCableCRM.Model
{
    public class UserCodeVerifyModel : BaseModel
    {
        public string OperatorCode { get; set; }
    }

    public class CodeVerifyResponse
    {
        public string status { get; set; }
        public string message { get; set; }
        public VerifiedData data { get; set; }
    }

    public class VerifiedData
    {
        public string operatoname { get; set; }
        public string operatorurl { get; set; }
        public string operatorogo { get; set; }
        public string operatorcode { get; set; }
        public string portalid { get; set; }
    }
}
