﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityCableCRM.Model
{
    public class MenuModel
    {
        public string MenuItem { get; set; }
        public string PageName { get; set; }
    }
}
