﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityCableCRM.Model
{
    public class PendingBillsModel : BaseModel
    {
        public string customerid { get; set; }
        public string year { get; set; }
    }
    public class PendingBillsResponse
    {
        public string status { get; set; }
        public string message { get; set; }
        public PendingBillsData[] data { get; set; }
    }

    public class PendingBillsData
    {
        public string month { get; set; }
        public string year { get; set; }
        public string billnumber { get; set; }
        public string paymentmode { get; set; }
        public string chequeddnumber { get; set; }
        public string status { get; set; }
        public string monthlyrental { get; set; }
        public string paidamount { get; set; }
        public string discountedamount { get; set; }
        public string discountnote { get; set; }
        public string pendingamount { get; set; }
    }

    public class ShowPendingBills
    {
        public string Month { get; set; }
        public string BillNumber { get; set; }
        public string PendingAmount { get; set; }
    }
}
