﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityCableCRM.Model
{
    public class LoginModel : BaseModel
    {
        public string username { get; set; }
        public string password { get; set; }
    }
    public class LoginResponse
    {
        public string status { get; set; }
        public string message { get; set; }
        public LoginData data { get; set; }
    }
    public class LoginData
    {
        public string loginstatus { get; set; }
        public string username { get; set; }
        public string userid { get; set; }
        public string messagecode { get; set; }
        public string message { get; set; }
        public string profilepictureurl { get; set; }
        public string customerid { get; set; }
    }

}
