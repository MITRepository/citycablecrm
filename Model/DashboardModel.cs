﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CityCableCRM.Common.Constants;

namespace CityCableCRM.Model
{
    public class DashboardModel : BaseModel
    {
        public string customerid { get; set; }
        public string month { get; set; }
        public string year { get; set; }
    }

    public class DashboardResponse
    {
        public string status { get; set; }
        public string message { get; set; }
        public DashboardData[] data { get; set; }
    }

    public class DashboardData
    {
        public string cust_id { get; set; }
        public string cust_name { get; set; }
        public string cust_location { get; set; }
        public string cust_address { get; set; }
        public string cust_mobile { get; set; }
        public string smart_number { get; set; }
        public string monthly_rental { get; set; }
        public string balance_amount { get; set; }
        public string month { get; set; }
        public string paymentstatus { get; set; }
        public string CustomerACNo { get; set; }
    }
}
